This repository contains an example of using cucumber to create BDD tests. 

General notes:

The steps you define in the feature file should have the exact spelling as the steps you define in your steps class in order to invoke the desired functions. 

If you have multiple feature files, each file has to have unique steps. 

You can define multiple scenarios in the same feature file.

You can run a specific feature by specifying the path to that feature in your runner class. 


